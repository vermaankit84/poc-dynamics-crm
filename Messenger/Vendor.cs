﻿using System.Runtime.Serialization;

namespace Messenger
{
    [DataContract]
    public class Vendor
    {
        [DataMember]
        public string Authorization { get; set; }
        [DataMember]
        public string Credentials { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Headers { get; set; }
        [DataMember]
        public string Requesttype { get; set; }
        public override string ToString()
        {
            return "Vendor Details Obtained [ " + Authorization + " ] , [ " + Credentials + " ] , [ " + Url + " ] , [ " + Headers + " ] , [ " + Requesttype + " ] ";
        }
    }
}
