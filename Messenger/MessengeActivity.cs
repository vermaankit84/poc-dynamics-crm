﻿using System;
using Microsoft.Xrm.Sdk;
using System.Activities;
using System.IO;
using System.Text;
using System.Threading;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System.Net;
using System.Runtime.Serialization.Json;


namespace Messenger
{
    public sealed class MessengeActivity : CodeActivity
    {
        [Input("Message")]
        [RequiredArgument]
        public InArgument<string> StrMessage { get; set; }

        [Input("Destination")]
        [RequiredArgument]
        public InArgument<string> StrDestination { get; set; }

        [Input("Message ID")]
        [RequiredArgument]
        public InArgument<string> StrMessageId { get; set; }

        protected override void Execute(CodeActivityContext codeActivityContext)
        {
            ITracingService tracingService = codeActivityContext.GetExtension<ITracingService>();
            IWorkflowContext context = codeActivityContext.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = codeActivityContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
            
            FilterExpression filterExpression = new FilterExpression();
            filterExpression.Conditions.Add(new ConditionExpression("new_active", ConditionOperator.Equal, false));
            
            QueryExpression queryExpression = new QueryExpression("new_vendor_details_json");
            queryExpression.ColumnSet = new ColumnSet(new string[] { "new_sms_settings" });
            queryExpression.Criteria.AddFilter(filterExpression);
            
            EntityCollection entityCollection = service.RetrieveMultiple(queryExpression);

            tracingService.Trace("No of vendor details Obtained [ " + entityCollection.Entities.Count + " ]");
            string strResponse = null;
            foreach (Entity vendorEntity in entityCollection.Entities)
            {                
                Vendor vendor = BuildVendor(vendorEntity.Attributes["new_sms_settings"].ToString());
                tracingService.Trace("Vendor Details Obtained [ " + vendor + " ] ");
                for (var i = 0; i < 3; i = i + 1)
                {
                    strResponse = SendMessage(vendor, StrMessage.Get<string>(codeActivityContext), StrDestination.Get<string>(codeActivityContext), tracingService);
                    if (!string.IsNullOrEmpty(strResponse))
                    {
                        Entity senderEntity = new Entity("new_sender_log");
                        senderEntity["new_message_id"] = StrMessageId.Get<string>(codeActivityContext);
                        senderEntity["new_message_text"] = StrMessage.Get<string>(codeActivityContext);
                        senderEntity["new_message_type"] = true;
                        senderEntity["new_status"] = "Send" + "-" + strResponse;
                        senderEntity["new_submit_date"] = DateTime.Today;
                        senderEntity["new_destination"] = StrDestination.Get<string>(codeActivityContext);
                        Guid vendorGuid = service.Create(senderEntity);
                        tracingService.Trace("Vendor Details Obtained [ " + vendorGuid + " ] at [ " + DateTime.Now + " ] ");
                        break;
                    }
                    else
                    {
                        tracingService.Trace("Message Sending Failed [ " + DateTime.Now + " ] ");
                        Thread.Sleep(5000);
                    }
                }

                if (!String.IsNullOrEmpty(strResponse))
                {
                    tracingService.Trace("Response Obtained [ " + strResponse + " ] at [ " + DateTime.Now +
                                         " ] for Message [ " + StrMessage.Get<string>(codeActivityContext) + " ] , [ " +
                                         StrDestination.Get<string>(codeActivityContext) + " ] ");
                    DeleteRecord(StrMessageId.Get<string>(codeActivityContext), service, tracingService);
                    StrRespone.Set(codeActivityContext, strResponse);
                    break;
                }
            }

        }

        private void DeleteRecord(string messageId , IOrganizationService service , ITracingService tracingService)
        {
            tracingService.Trace("Delete Record from Message Queue Started [ " + DateTime.Now + " ] ");
            FilterExpression filterExpression = new FilterExpression();
            filterExpression.Conditions.Add(new ConditionExpression("new_message_id", ConditionOperator.Equal, messageId));

            QueryExpression queryExpression = new QueryExpression("new_message_queue");
            queryExpression.ColumnSet = new ColumnSet(new string[] { "new_message_queueid" });
            queryExpression.Criteria.AddFilter(filterExpression);

            EntityCollection entityCollection = service.RetrieveMultiple(queryExpression);
            foreach (Entity messageEntity in entityCollection.Entities)
            {
               Guid messageGuid =  Guid.Parse(messageEntity.Attributes["new_message_queueid"].ToString());
               service.Delete("new_message_queue", messageGuid);
            }
            tracingService.Trace("Delete Record from Message Queue Ended [ " + DateTime.Now + " ] ");
        }


        private Vendor BuildVendor(string vendorData)
        {
            DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(Vendor));
            MemoryStream memoryStream =  new MemoryStream();
            StreamWriter streamWriter = new StreamWriter(memoryStream);
            streamWriter.Write(vendorData);
            streamWriter.Flush();
            memoryStream.Position = 0;
            return (Vendor)(dataContractJsonSerializer.ReadObject(memoryStream));
        }

        private string SendMessage(Vendor vendorDetails, string message, string destination, ITracingService tracingService)
        {
            Stream reqStream = null;
            WebResponse webResponse = null;
            try
            {
                WebRequest webRequest = WebRequest.Create(vendorDetails.Url.Trim());
                webRequest.Method = String.IsNullOrEmpty(vendorDetails.Requesttype) ? "post".ToUpper() : vendorDetails.Requesttype;
                webRequest.Headers.Clear();
                webRequest.ContentType = vendorDetails.Headers;
                string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(vendorDetails.Authorization.Trim()));
                webRequest.Headers.Add("Authorization", "Basic " + encoded);
                string postData = vendorDetails.Credentials.Replace("%To%", destination).Replace("%Message%", WebUtility.UrlEncode(message.Trim()));
                tracingService.Trace("Post Data Obtained [ " + postData + " ] ");
                reqStream = webRequest.GetRequestStream();
                byte[] postArray = Encoding.ASCII.GetBytes(postData);
                reqStream.Write(postArray, 0, postArray.Length);
                webResponse = webRequest.GetResponse();
                String strResponse = null;
                HttpWebResponse httpWebResponse = (HttpWebResponse)webResponse;
                tracingService.Trace("Web response Obtained [ " + httpWebResponse + " ] at [ " + DateTime.Now + " ] ");
                if ((long)httpWebResponse.StatusCode == 201)
                {
                    strResponse = "Send";
                }
                return strResponse;
            }
            catch (Exception e)
            {
                tracingService.Trace("Send Message Failed [ " + e.Message + " ] ");
                return null;
            }
            finally
            {
                if (reqStream != null)
                {
                    reqStream.Close();

                }

                if (webResponse != null)
                {
                    webResponse.Close();
                }
            }
        }

        public OutArgument<string> StrRespone { get; set; }

    }
}
